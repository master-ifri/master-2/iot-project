## IFRI Master 2 Génie Logiciel 2021-2022 : Projet Final IOT

Pour la réalisation de ce projet, nous avons eu à utiliser certaines ressources que voici :

- Le fichier "border-router.c" du dossier "border-router"
Nous l'avons utilisé pour créer un router.

- Le fichier "sky-websense.c" du dossier "sky-websense"
Nous l'avons utilisé pour créer un capteur de proximité.

- Le fichier "hello-world.c" du dossier "hello-world"
Nous l'avons utilisé pour créer un capteur de lumière.

- Le fichier "collect-view-shell.c" du dossier "collect"
Nous l'avons utilisé pour recevoir les messages de tous les capteurs, et aussi pour envoyer des commandes à tout les noeuds.

* Le fichier de simulation "borderRouter.csc" 
Ce fichier contient le rendu d'une simulation fait avec le capteur de lumière.


### Auteurs de ce projet

- ZOSSOU Beaudelaire
- N'TIA Théodule
- TCHANHOUIN Amède